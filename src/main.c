#include <time.h>
#include <string.h>
#include <util/delay.h>
#include <avr/pgmspace.h>
#include <util/atomic.h>
#include <avr/interrupt.h>
#include <stdlib.h> // stdlib is needed to use ltoa() - Long to ASCII
#include "../lib/andygock_avr-uart/uart.h"
#include "../lib/helius_microrl/microrl.h"
#include "../lib/hd44780_111/hd44780.h"
#include "cli_microrl.h"
#include "print_helper.h"
#include "hmi_msg.h"
#include "../lib/matejx_avr_lib/mfrc522.h"
#include "../lib/andy_brown_memdebug/memdebug.h"

#define UART_BAUD   9600
#define UART_STATUS_MASK    0x00FF
#define BLINK_DELAY_MS  900

typedef enum {
    door_opening,
    door_open,
    door_closing,
    door_closed
} door_state_t;
door_state_t door_state = door_closed;
volatile uint32_t system_time;
// Create microrl object and pointer on it
static microrl_t rl;
static microrl_t *prl = &rl;


static inline void init_rfid_reader(void)
{
    /*  Init    RFID-RC522  */
    MFRC522_init();
    PCD_Init();
}



static inline void init_micro(void)
{
    // Call init with ptr to microrl instance and print callback
    microrl_init (prl, uart0_puts);
    // Set callback for execute
    microrl_set_execute_callback (prl, cli_execute);
}

static inline void init_leds(void)
{
    /* Set port A pin 0 for output for red LED */
    DDRA |= _BV(DDA0);
    /* Set port A pin 2 for output for green LED */
    DDRA |= _BV(DDA2);
    /* Set port A pin 4 for output for blue LED */
    DDRA |= _BV(DDA4);
    /* Set port B pin 7 for output for Arduino Mega yellow LED */
    DDRB |= _BV(DDB7);
    /*Turn off the Arduino Mega yellow LED */
    PORTB &= ~_BV(PORTB7);
}


static inline void init_con_uart1(void)
{
    uart1_puts_p(PSTR(VER_FW));
    uart1_puts_p(PSTR(VER_LIBC));
}

static inline void init_sys_timer(void)
{
    //    counter_1 = 0; // Set counter to random number 0x19D5F539 in HEX. Set it to 0 if you want
    TCCR1A = 0;
    TCCR1B = 0;
    TCCR1B |= _BV(WGM12); // Turn on CTC (Clear Timer on Compare)
    TCCR1B |= _BV(CS12); // fCPU/256
    OCR1A = 62549; // Note that it is actually two registers OCR5AH and OCR5AL
    TIMSK1 |= _BV(OCIE1A); // Output Compare A Match Interrupt Enable
}



static inline void hw_init()
{
    DDRD |= _BV(DDD3);
    uart0_init(UART_BAUD_SELECT(UART_BAUD, F_CPU));
    uart1_init(UART_BAUD_SELECT(UART_BAUD, F_CPU));
    init_sys_timer();
    sei();
    lcd_init();
    lcd_clrscr();
}


static inline void heartbeat(void)
{
    static time_t prev_time;
    char ascii_buf[11] = {0x00};
    time_t now = time(NULL);

    if (prev_time != now) {
        //Print uptime to uart1
        ltoa(now, ascii_buf, 10);
        uart1_puts_p(PSTR("Uptime: "));
        uart1_puts(ascii_buf);
        uart1_puts_p(PSTR(" s.\r\n"));
        //Toggle LED
        PORTA ^= _BV(PORTA0);
        PORTA ^= _BV(PORTA2);
        //PORTA ^= _BV(PORTA4);
        prev_time = now;
    }
}


static inline uint32_t cur_time(void)
{
    uint32_t current_time;
    ATOMIC_BLOCK(ATOMIC_FORCEON) {
        current_time = system_time;
    }
    return current_time;
}

static inline void door_status()
{
    switch (door_state) {
    case door_opening:
        DDRA ^= _BV(DDA2);
        PORTA |= _BV(PORTA0);
        door_state = door_open;
        break;

    case door_open:
        break;

    case door_closing:
        door_state = door_closed;
        PORTA &= ~_BV(PORTA0);
        DDRA ^= _BV(DDA2);
        break;

    case door_closed:
        break;
    }
}


static inline void card_handle()
{
    Uid uid;
    Uid *uid_ptr = &uid;
    int status;
    uint32_t time_cur = cur_time();
    static uint32_t mstart;
    static uint32_t open_start;
    static uint32_t read_count;
    static char *name;
    static int mstat;
    static int open_stat;
    char *uname;
    byte bufferATQA[10];
    byte bufferSize[10];

    if ((read_count + 1) < time_cur) {
        if (PICC_IsNewCardPresent()) {
            read_count = time_cur;
            PICC_ReadCardSerial(&uid);
            uname = tallymarker2hex(uid_ptr->uidByte, uid_ptr->size);
            card_t *current = head;

            while (current != NULL) {
                if (strcmp(current->uid, uname) == 0) {
                    status = 1;
                    break;
                }

                status = 0;
                current = current->next;
            }

            if (status == 1) {
                if (door_state != door_open) {
                    open_stat = 1;
                    door_state = door_opening;
                }

                if (current->user != name || name == NULL) {
                    status = 2;
                    lcd_clr(LCD_ROW_2_START, LCD_VISIBLE_COLS);
                    lcd_goto(LCD_ROW_2_START);
                    lcd_puts(current->user);
                    name = current->user;
                }

                open_start = time_cur;
            } else {
                if (status != 3) {
                    status = 3;
                    lcd_clr(LCD_ROW_2_START, LCD_VISIBLE_COLS);
                    lcd_goto(LCD_ROW_2_START);
                    lcd_puts_P(PSTR("No rights"));
                    name = NULL;
                }

                if (door_state != door_closed) {
                    open_stat = 0;
                    door_state = door_closing;
                }
            }

            free(uname);
            mstat = 1;
            mstart = time_cur;
            PICC_WakeupA(bufferATQA, bufferSize);
        }
    }

    if ((open_start + 3) < time_cur && open_stat) {
        door_state = door_closing;
        open_stat = 0;
    }

    if ((mstart + 5) < time_cur && mstat) {
        lcd_clr(LCD_ROW_2_START, LCD_VISIBLE_COLS);
        status = 2;
        name = NULL;
        mstat = 0;
    }

    door_status(door_state);
}




void main(void)
{
    init_leds();
    DDRD |= _BV(DDD3);
    uart0_init(UART_BAUD_SELECT(UART_BAUD, F_CPU));
    uart1_init(UART_BAUD_SELECT(UART_BAUD, F_CPU));
    init_sys_timer();
    sei(); // Enable all interrupts. Set up all interrupts before sei()!!!
    lcd_init();
    uart0_puts_p(PSTR(SNAME));
    lcd_puts_P(PSTR(SNAME));
    init_micro();
    init_con_uart1();
    init_rfid_reader();

    while (1) {
        card_handle();
        heartbeat();
        microrl_insert_char(prl, (uart0_getc() & UART_STATUS_MASK));
    }
}

ISR(TIMER1_COMPA_vect)
{
    //system_tick();
    system_time++;
}
