#include <ctype.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <string.h>
#include <avr/pgmspace.h>
#include "../lib/hd44780_111/hd44780.h"
#include "../lib/andygock_avr-uart/uart.h"
#include "../lib/helius_microrl/microrl.h"
#include "../lib/matejx_avr_lib/mfrc522.h"
#include "../lib/andy_brown_memdebug/memdebug.h"
#include "hmi_msg.h"
#include "cli_microrl.h"
#include "print_helper.h"
#define NUM_ELEMS(x)        (sizeof(x) / sizeof((x)[0]))

void cli_print_help(const char *const *argv);
void cli_example(const char *const *argv);
void cli_print_ver(const char *const *argv);
void cli_print_ascii_tbls(const char *const *argv);
void cli_handle_number(const char *const *argv);
void cli_print_cmd_error(void);
void cli_print_cmd_arg_error(void);
void cli_rfid_read(const char *const *argv);
void cli_rfid_list(const char *const *argv);
void cli_rfid_add(const char *const *argv);
void cli_rfid_remove(const char *const *argv);
void cli_mem_stat(const char *const *argv);


typedef struct cli_cmd {
    PGM_P cmd;
    PGM_P help;
    void (*func_p)();
    const uint8_t func_argc;
} cli_cmd_t;

const char help_cmd[] PROGMEM = "help";
const char help_help[] PROGMEM = "Get help";
const char example_cmd[] PROGMEM = "example";
const char example_help[] PROGMEM =
    "Prints out all provided 3 arguments Usage: example <argument> <argument> <argument>";
const char ver_cmd[] PROGMEM = "version";
const char ver_help[] PROGMEM = "Print FW version";
const char ascii_cmd[] PROGMEM = "ascii";
const char ascii_help[] PROGMEM = "Print ASCII tables";
const char rfid_cmd[] PROGMEM = "rfid";
const char rfid_help[] PROGMEM = "rfid help";
const char number_cmd[] PROGMEM = "number";
const char number_help[] PROGMEM =
    "Print and display matching number Usage: number <decimal number>";
const char rfid_list_cmd[] PROGMEM = "list";
const char rfid_list_help[] PROGMEM = "list help";
const char rfid_add_cmd[] PROGMEM = "add";
const char rfid_add_help[] PROGMEM = "add help";
const char rfid_remove_cmd[] PROGMEM = "remove";
const char rfid_remove_help[] PROGMEM = "remove help";
const char mem_stat_cmd[] PROGMEM = "mem";
const char mem_stat_help[] PROGMEM =
    "Print memory usage and change compared to previous call";



const cli_cmd_t cli_cmds[] = {
    {help_cmd, help_help, cli_print_help, 0},
    {ver_cmd, ver_help, cli_print_ver, 0},
    {example_cmd, example_help, cli_example, 3},
    {ascii_cmd, ascii_help, cli_print_ascii_tbls, 0},
    {number_cmd, number_help, cli_handle_number, 1},
    {rfid_cmd, rfid_help, cli_rfid_read, 0},
    {rfid_list_cmd, rfid_list_help, cli_rfid_list, 0},
    {rfid_add_cmd, rfid_add_help, cli_rfid_add, 2},
    {rfid_remove_cmd, rfid_remove_help, cli_rfid_remove, 1},
    {mem_stat_cmd, mem_stat_help, cli_mem_stat, 0}
};


void cli_print_help(const char *const *argv)
{
    (void) argv;
    uart0_puts_p(PSTR("Implemented commands:\r\n"));

    for (uint8_t i = 0; i < NUM_ELEMS(cli_cmds); i++) {
        uart0_puts_p(cli_cmds[i].cmd);
        uart0_puts_p(PSTR(" : "));
        uart0_puts_p(cli_cmds[i].help);
        uart0_puts_p(PSTR("\r\n"));
    }
}

void cli_rfid_read(const char *const *argv)
{
    (void) argv;
    Uid uid;
    Uid *uid_ptr = &uid;
    uart0_puts_p(PSTR("\n"));

    if (!PICC_IsNewCardPresent()) {
        byte bufferATQA[10];
        byte bufferSize = sizeof(bufferATQA);
        byte result;
        result = PICC_WakeupA(bufferATQA, &bufferSize);

        if (result != STATUS_OK) {
            uart0_puts_p((PSTR("Unable to select card.\r\n")));
            return;
        }
    }

    uart0_puts_p(PSTR("Card selected!\r\n"));
    PICC_ReadCardSerial(uid_ptr);
    uart0_puts_p(PSTR("UID type: "));
    uart0_puts(PICC_GetTypeName(PICC_GetType(uid.sak)));
    uart0_puts_p(PSTR("\r\n"));
    uart0_puts_p(PSTR("Card UID: "));
    uart0_puts(tallymarker2hex(uid.uidByte, uid.size));
    uart0_puts_p(PSTR("\r\n"));
    uart0_puts_p(PSTR("UID size: "));
    uart0_puts(tallymarker2hex(&uid.size, sizeof (uid.size)));;
    uart0_puts_p(PSTR("\r\n"));
}
void cli_example(const char *const *argv)
{
    uart0_puts_p(PSTR("Command had following arguments:\r\n"));

    for (uint8_t i = 1; i < 4; i++) {
        uart0_puts(argv[i]);
        uart0_puts_p(PSTR("\r\n"));
    }
}

void cli_print_ver(const char *const *argv)
{
    (void) argv;
    uart0_puts_p(PSTR(VER_FW));
    uart0_puts_p(PSTR(VER_LIBC));
}


void cli_print_ascii_tbls(const char *const *argv)
{
    (void) argv;
    print_ascii_tbl();
    unsigned char ascii[128] = {0};

    for (unsigned char i = 0; i < sizeof(ascii); i++) {
        ascii[i] = i;
    }

    print_for_human (ascii, sizeof(ascii));
}


void cli_handle_number(const char *const *argv)
{
    (void) argv;
    int ent_no = atoi(argv[1]);
    lcd_clr(LCD_ROW_2_START, LCD_VISIBLE_COLS);
    lcd_goto(LCD_ROW_2_START);

    for (size_t i = 0; i < strlen(argv[1]); i++) {
        if (!isdigit(argv[1][i])) {
            uart0_puts_p(PSTR("Argument is not a decimal number!\r\n"));
            lcd_puts_P(PSTR(NOTNUMB));
            return;
        }
    }

    if (ent_no >= 0 && ent_no < 10) {
        uart0_puts_p(PSTR(ANUMB));
        uart0_puts_p((PGM_P)pgm_read_word(&(eng_numbers[ent_no])));
        uart0_puts_p(PSTR("\r\n"));
        lcd_goto(0x40);
        lcd_puts_P((PGM_P)pgm_read_word(&(eng_numbers[ent_no])));
    } else {
        uart0_puts_p(PSTR(ERRNUMB));
        lcd_puts_P(PSTR(BIGNUMB));
    }
}


void cli_print_cmd_error(void)
{
    uart0_puts_p(PSTR("Command not implemented.\r\n\tUse <help> to get help.\r\n"));
}


void cli_print_cmd_arg_error(void)
{
    uart0_puts_p(
        PSTR("To few or too many arguments for this command\r\n\tUse <help>\r\n"));
}


void cli_rfid_add(const char *const *argv)
{
    char *name;
    char *new_card_size;
    uint8_t uid_length;
    char *new_card_uid;
    name = malloc(strlen(argv[1]) + 1);
    strcpy(name, argv[1]);
    uid_length = strlen(name) / 2;

    if (uid_length > 10) {
        uart0_puts_p(PSTR("UID size is too long\r\n"));
        uart0_puts_p(PSTR("\r\n"));
        free(name);
        return;
    }

    card_t *current = head;

    while (current != NULL) {
        if (strcmp(current->uid, name) == 0) {
            uart0_puts_p(PSTR("Can't add card again\n"));
            uart0_puts_p(PSTR("\r\n"));
            return;
        }

        current = current-> next;
    }

    card_t *new_card = malloc(sizeof(card_t));
    char *new_card_user;

    if (!new_card) {
        uart0_puts_p(PSTR(" No memory to add\n"));
        free(new_card_user);
        free(new_card);
        return;
    }

    new_card_user = malloc(strlen(argv[2]));
    new_card_uid = malloc(strlen(argv[1]));
    new_card_size = malloc(uid_length + 1);
    strcpy(new_card_size, itoa (uid_length, new_card_size, 10));
    strcpy (new_card_user, argv[2]);
    strcpy(new_card_uid, argv[1]);
    new_card->uid = new_card_uid;
    new_card->uid_size = new_card_size;
    new_card->user = new_card_user;
    new_card->next = NULL;

    if (head == NULL) {
        head = new_card;
        uart0_puts_p(PSTR("Card added"));
        uart0_puts_p(PSTR("\r\n"));
    } else {
        card_t *temp = head;

        while (temp->next != NULL) {
            temp = temp->next;
        }

        temp->next = new_card;
        uart0_puts_p(PSTR("Card added"));
        uart0_puts_p(PSTR("\r\n"));
    }

    uart0_puts_p(PSTR(" UID is: "));
    uart0_puts(new_card->uid);
    uart0_puts_p(PSTR(" Card size is: "));
    uart0_puts(new_card->uid_size);
    uart0_puts_p(PSTR(" User: "));
    uart0_puts(new_card->user);
    uart0_puts_p(PSTR("\r\n"));
}



void cli_rfid_remove(const char *const *argv)
{
    char *uid_value = malloc(strlen(argv[1]));
    strcpy(uid_value, argv[1]);
    card_t *previous = head;

    if (head == NULL) {
        uart0_puts_p(PSTR("List is empty\r\n"));
        return;
    }

    card_t * current = head;

    if (strcmp(current-> uid, uid_value) == 0) {
        head = current-> next;
        free(current);
        uart0_puts_p(PSTR("Card removed \r\n"));
        return;
    }

    while (current != NULL) {
        if (strcmp(current->uid, uid_value) == 0) {
            previous->next = current->next;
            free(uid_value);
            uart0_puts_p(PSTR("Card removed\r\n"));
            return;
        }

        previous = current;
        current = current->next;
    }

    if (current == NULL || current->next == NULL) {
        uart0_puts_p(PSTR("No such card\r\n"));
    }

    return;
    card_t *next = current-> next -> next;
    free(current-> next);
    current -> next = next;
}

card_t *head = NULL;
void cli_rfid_list(const char *const *argv)
{
    (void) argv;
    card_t *current = head;
    int count = 1;

    if (head == NULL) {
        uart0_puts(PSTR("No cards in the list"));
        uart0_puts_p(PSTR("\r\n"));
    } else {
        while (current != NULL) {
            char *c;
            uart0_puts_p(PSTR("Number: "));
            uart0_puts(itoa(count, c, 10));
            uart0_puts_p(PSTR(" UID is: "));
            uart0_puts(current->uid);
            uart0_puts_p(PSTR(" Card size is: "));
            uart0_puts(current->uid_size);
            uart0_puts_p(PSTR(" User: "));
            uart0_puts(current->user);
            uart0_puts_p(PSTR("\r\n"));
            count++;
            current = current->next;
        }
    }
}


void cli_mem_stat(const char *const *argv)
{
    (void) argv;
    char print_buf[256] = {0x00};
    extern int __heap_start, *__brkval;
    int v;
    int space;
    static int prev_space;
    space = (int) &v - (__brkval == 0 ? (int) &__heap_start : (int) __brkval);
    uart0_puts_p(PSTR("Heap statistics\r\n"));
    sprintf_P(print_buf, PSTR("Used: %u\r\nFree: %u\r\n"), getMemoryUsed(),
              getFreeMemory());
    uart0_puts(print_buf);
    uart0_puts_p(PSTR("\r\nSpace between stack and heap:\r\n"));
    sprintf_P(print_buf, PSTR("Current  %d\r\nPrevious %d\r\nChange   %d\r\n"),
              space, prev_space, space - prev_space);
    uart0_puts(print_buf);
    uart0_puts_p(PSTR("\r\nFreelist\r\n"));
    sprintf_P(print_buf, PSTR("Freelist size:             %u\r\n"),
              getFreeListSize());
    uart0_puts(print_buf);
    sprintf_P(print_buf, PSTR("Blocks in freelist:        %u\r\n"),
              getNumberOfBlocksInFreeList());
    uart0_puts(print_buf);
    sprintf_P(print_buf, PSTR("Largest block in freelist: %u\r\n"),
              getLargestBlockInFreeList());
    uart0_puts(print_buf);
    sprintf_P(print_buf, PSTR("Largest freelist block:    %u\r\n"),
              getLargestAvailableMemoryBlock());
    uart0_puts(print_buf);
    sprintf_P(print_buf, PSTR("Largest allocable block:   %u\r\n"),
              getLargestNonFreeListBlock());
    uart0_puts(print_buf);
    prev_space = space;
}

int cli_execute(int argc, const char *const *argv)
{
    uart0_puts_p(PSTR("\r\n"));

    for (uint8_t i = 0; i < NUM_ELEMS(cli_cmds); i++) {
        if (!strcmp_P(argv[0], cli_cmds[i].cmd)) {
            // Test do we have correct arguments to run command
            // Function arguments count shall be defined in struct
            if ((argc - 1) != cli_cmds[i].func_argc) {
                cli_print_cmd_arg_error();
                return 0;
            }

            // Hand argv over to function via function pointer,
            // cross fingers and hope that funcion handles it properly
            cli_cmds[i].func_p (argv);
            return 0;
        }
    }

    cli_print_cmd_error();
    return 0;
}



