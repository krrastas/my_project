#include <avr/pgmspace.h>
#ifndef _HMI_MSG_H_
#define _HMI_MSG_H_

const char c0[] PROGMEM = "Zero";
const char c1[] PROGMEM = "One";
const char c2[] PROGMEM = "Two";
const char c3[] PROGMEM = "Three";
const char c4[] PROGMEM = "Four";
const char c5[] PROGMEM = "Five";
const char c6[] PROGMEM = "Six";
const char c7[] PROGMEM = "Seven";
const char c8[] PROGMEM = "Eight";
const char c9[] PROGMEM = "Nine";

PGM_P const eng_numbers[] PROGMEM = {c0, c1, c2, c3, c4, c5, c6, c7, c8, c9};
#endif
