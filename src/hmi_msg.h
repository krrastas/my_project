#ifndef _HMI_MSG_H_
#define _HMI_MSG_H_

#define SNAME "Kristina Rastas \r\n"
#define VER_FW   "Version: " FW_VERSION " built on: " __DATE__ " " __TIME__ "\r\n"
#define VER_LIBC "avr-libc version: " __AVR_LIBC_VERSION_STRING__ " avr-gcc version: " __VERSION__ "\r\n"

#define ENUMB "Enter number > \r\n"
#define ANUMB "You entered number "
#define ERRNUMB "Please enter number between 0 and 9! \r\n"
#define BIGNUMB "Too big number"
#define NOTNUMB "Not number"


extern PGM_P const eng_numbers[];

#endif
