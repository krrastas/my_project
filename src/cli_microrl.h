#ifndef CLI_MICRORL_H
#define CLI_MICRORL_H
typedef struct card {
    char *uid;
    char *user;
    char *uid_size;
    struct card *next;
} card_t;

/* Execute callback */
int cli_execute(int argc, const char *const *argv);
extern card_t *head;
void cli_rfid_list (const char *const *argv);
void cli_rfid_remove (const char *const *argv);
#endif 
